# Spike Report

## SPIKE HD - NVIDIA GAMEWORKS CLOTH

### Introduction

We Created Cloth using the Unreal Engine. Wanting more control over cloth we want to look at The NVIDIA Game works implantation of cloth.

### Goals

* The Spike Report should answer each of the Gap questions

* Sign Up Nvidia and Unreal github
* Compile the nVidia GameWorks Flex branch of Unreal engine

* In a new project
  * Make Different types of cloth objects
    * One That Tears
    * Stiff cloth
    * One that Attaches to other objects.

### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Jacob Pipers
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* [FleX Unreal Source](https://github.com/NvPhysX/UnrealEngine/tree/FleX-4.16.0#getting-up-and-running)
* NVidia GameWorks
* [Flex Documentation](http://docs.nvidia.com/gameworks/content/artisttools/Flex/FLEXUe4_Intro.html)
* Visual Studio
* Unreal Engine

### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

* Sign Up Unreal github [more info](https://www.youtube.com/watch?v=usjlNHPn-jo)

* Sign Up NVIDI unreal github [Login / Join here](https://developer.nvidia.com/gameworks-source-github)

* build Flex branch of unreal Nvidia Follow the Getting up and running [readme](https://github.com/NvPhysX/UnrealEngine/tree/FleX-4.16.0#getting-up-and-running)

* Flex Project Example

  * Open Static Mesh Asset To make Cloth behaviour

  * Toggle the Flex Option

  ![](Images/FlexStaticMesh.png)

  * Set Flex asset to Flex Cloth Asset as seen below.

  ![](Images/FlexAssetImage.png)

  * Make a new Flex Container as seen image below.

  ![](Images/Container.png)

  * Create a Sphere mesh to use as a cloth ball.
    * In Details in Flex Asset Details in the Static Mesh set Inflatable to true.

  ![ball](Images/ball.gif)

  * Tearing Cloth
    * In Details of Static Mesh Set Tearing to true.
    * Set Tearing Max Strain to 2.5 and Break rate to 95. These valus can be played with.

  ![Tearing](Images/attach.gif)

  * Attaching to rigids
    * In Details set Attach to rigids to true.

  ![Attach](Images/tear.gif)

### What we found out

### [Optional] Open Issues/Risks

List out the issues and risks that you have been unable to resolve at the end of the spike. You may have uncovered a whole range of new risks as well.

* As this feture is not part of the main release of unreal: Compiling the Nvidia flex branch of unreal required some changes in some windows sdks to get the engine to compile.

* Still early technology and will only work on certain GPUs as of this writting. - GPUS: NVidia cards with Compute Capability 3.0 or higher (Kepler) . Driver: NVIDIA driver of at least version 304.54 on Linux, and 306.94 on Windows.

* Attaching requires that rigid be overlapping.

* Cloth collision works but accurecy was lower as was not able to get to collide with mesh instead root capsule component collides.

### [Optional] Recommendations

You may state that another spike is required to resolve new issues identified (or) indicate that this spike has increased the team�s confidence in XYZ and move on.

To collide with mesh. with collisions.